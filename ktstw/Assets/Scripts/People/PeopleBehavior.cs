﻿/**
*	Created 01-26-2018
* 	Author : Vegevan
**/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Scripts.GameManagers;

using Scripts.Level;

namespace Scripts.People
{

	public class PeopleBehavior : MonoBehaviour 
	{
		/// <summary>
		/// Left button index
		/// </summary>
		const int kLeftButtonIndex = 0;
		
		public SpriteRenderer _FaceRenderer = null;
		public SpriteRenderer _MouthRenderer = null;
		public SpriteRenderer _EyesRenderer = null;
		public SpriteRenderer _EyebrowsRenderer = null;
		public SpriteRenderer _VaccinRenderer = null;
		public SpriteRenderer _SickRenderer = null;

		public List<Sprite> _Faces;
		public List<Sprite> _Eyes;
		public List<Sprite> _Eyebrows;
		public List<Sprite> _Mouthes;

		public List<Color> _PossibleColors;

		/// <summary>
		/// Sprite renderer
		/// </summary>
		new internal SpriteRenderer renderer {get {return _FaceRenderer;}}

		/// <summary>
		/// Used by Deseases
		/// >0 : Sick
		/// +0 : Default
		/// -1 : DEAD
		/// </summary>
		int m_deseaseState = 0;
		internal int deseaseState {get {return m_deseaseState;}}
		internal int turnsLeftVaccined = 0;

		/// <summary>
		/// Position in map coordinate system
		/// </summary>
		internal Vector2 mapPosition;

		internal Color baseColor{get; private set;}

		/// <summary>
		/// Awake
		/// </summary>
		void Awake()
		{
			Debug.Assert(_FaceRenderer != null && _EyebrowsRenderer != null && _EyesRenderer != null && _MouthRenderer != null && _VaccinRenderer != null && _SickRenderer != null);

			_MouthRenderer.sprite		= _Mouthes	[ Random.Range(0, _Mouthes.Count)];
			_EyebrowsRenderer.sprite 	= _Eyebrows	[ Random.Range(0, _Eyebrows.Count)];
			_EyesRenderer.sprite 		= _Eyes		[ Random.Range(0, _Eyes.Count)];
			_FaceRenderer.sprite 		= _Faces	[ Random.Range(0, _Faces.Count)];

			_VaccinRenderer.gameObject.SetActive(false);
			_SickRenderer.gameObject.SetActive(false);

			baseColor = _PossibleColors[ Random.Range(0, _PossibleColors.Count)];
			_FaceRenderer.color = baseColor;			
		}

		/// <summary>
		/// Update
		/// </summary>
		void Update() // dirty
		{
			if (Managers.instance.inputManager.isSelected(renderer.bounds) == true)
			{
				Managers.instance.inputManager.OnClicked(this);

				if (renderer.color == Color.red)
				{
					renderer.color = baseColor;
				}
			}
		}

		/// <summary>
		/// Set desease state
		/// </summary>
		internal void setDeseaseState(int p_state)
		{
			m_deseaseState = p_state;
		}

		/// <summary>
		/// Callback - Turn is over
		/// </summary>
		internal void OnTurnOver()
		{
			// Vaccine management
			turnsLeftVaccined = Mathf.Max(0, turnsLeftVaccined - 1);
			_VaccinRenderer.gameObject.SetActive(turnsLeftVaccined > 0);
		}

		/// <summary>
		/// Callback - Infected
		/// </summary>
		internal void OnInfected()
		{
		}

		/// <summary>
		/// Callback - Vaccined
		/// </summary>
		internal void OnVaccined()
		{
		}

		/// <summary>
		/// Reveal
		/// </summary>
		internal void reveal()
		{
			if (m_deseaseState > 0)
			{
				_SickRenderer.gameObject.SetActive(true);
				renderer.color = Color.red;
			}

			if (turnsLeftVaccined > 0)
			{
				_VaccinRenderer.gameObject.SetActive(true);
			}
		}

		/// <summary>
		/// Hide status
		/// </summary>
		internal void hide()
		{
			_SickRenderer.gameObject.SetActive(false);
			_VaccinRenderer.gameObject.SetActive(false);
			renderer.color = baseColor;
		}

		/// <summary>
		/// Tells to move
		/// </summary>
		internal bool move(MapBehavior p_map)
		{
			int minX = (int)(p_map._Origin.x);
			int maxX = (int)(p_map._Origin.x + p_map._Dimensions.x);

			int maxY = (int)(p_map._Origin.y);
			int minY = (int)(p_map._Origin.y + p_map._Dimensions.y);

			List<Vector2> directions = new List<Vector2>();

			List<PeopleBehavior> neighbors = p_map.getNeighbors(this); // HAS FOUR ELEMENT FOR SURE	
			
			if (neighbors[0] == null && mapPosition.x > minX)
				directions.Add(Vector2.left);

			if (neighbors[1] == null && mapPosition.x < maxX)
				directions.Add(Vector2.right);

			if (neighbors[2] == null && mapPosition.y < maxY)
				directions.Add(Vector2.down);

			if (neighbors[3] == null && mapPosition.y > minY)
				directions.Add(Vector2.up);
		
			if (directions.Count == 0)
				return false;
			else
			{
				// Debug.LogFormat("old map position {0}", mapPosition);
				p_map.movePeople(mapPosition, mapPosition + directions[Random.Range(0, directions.Count)]);
			}
			// Debug.LogFormat("new map position {0}", mapPosition);
			return directions.Count != 0;
		}
	}
}