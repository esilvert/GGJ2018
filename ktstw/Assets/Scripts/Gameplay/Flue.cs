/**
*	Created 01-26-2018
* 	Author : Vegevan
**/

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using Scripts.People;

namespace Scripts.Gameplay
{
	public class Flue : Desease 
	{   
        enum EFlueStates
        {
            NonInfected = 0,
            Infected = 1

        }

        /// <summary>
        /// Number of first infected
        /// </summary>
        public int _NumberOfFirstInfected = 2;

		/// <summary>
		/// Called on first iteration
		/// </summary>
		protected override void firstIteration()
        {
            for(int i = 0 ; i < _NumberOfFirstInfected ; ++i)
            {
                PeopleBehavior people = _MapBehavior.getRandomPeople((int)EFlueStates.NonInfected);
                if (people)
                    infect(people);
            }
        }

		/// <summary>
		/// Next iteration
		/// </summary>
		protected override void iterate()
        {
            List<PeopleBehavior> infected = _MapBehavior.getAllPeopleWithState((int)EFlueStates.Infected);
            bool hasInfectedSomeone = false;

            foreach(PeopleBehavior people in infected)
            {
                List<PeopleBehavior> neighbors = _MapBehavior.getNeighbors(people);

                foreach(PeopleBehavior neighbor in neighbors)
                {
                    if (neighbor != null)
                        hasInfectedSomeone |= infect(neighbor);
                }
            }

            if (hasInfectedSomeone == false)
            {
                int i = 0;
                const int maxIdx = 1000;

                while (++i < maxIdx && infected.Count != 0)
                {
                    PeopleBehavior pb = infected[ Random.Range(0, infected.Count)];
                    if (pb.move(_MapBehavior) == true)
                    {
                        break;
                    }
                }
            }
        }

		/// <summary>
		/// Infects people
		/// </summary>
		protected override bool infect(PeopleBehavior p_target)
        {
            // Quick exit - Vaccined
            if (p_target.turnsLeftVaccined > 0)
            {
                return false;
            }

            switch(p_target.deseaseState)
            {
                case (int)EFlueStates.NonInfected:
                {
                    p_target.setDeseaseState((int)EFlueStates.Infected);
                    p_target.OnInfected();
                    return true;
                }

                case (int)EFlueStates.Infected:
                {

                }break;

                default: break;
            }

            return false;
        }

        /// <summary>
        /// Win conditions
        /// </summary>
		internal override bool hasDeseaseWon()
        {
            List<PeopleBehavior> infected = _MapBehavior.getAllPeopleNotInState((int)EFlueStates.Infected);
            return infected.Count == 0;
        }

        /// <summary>
        /// Lose condition
        /// </summary>
		internal override bool hasDeseaseLost()
        {
            List<PeopleBehavior> infected = _MapBehavior.getAllPeopleWithState((int)EFlueStates.Infected);
            return infected.Count == 0;
        }
	}
}