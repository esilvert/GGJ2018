/**
*	Created 01-26-2018
* 	Author : Vegevan
**/

using UnityEngine;

using Scripts.Level;
using Scripts.People;

namespace Scripts.Gameplay
{
	public abstract class Desease : MonoBehaviour 
	{
		/// <summary>
		/// The Map behavior
		/// </summary>
		public MapBehavior _MapBehavior = null;

		/// <summary>
		/// True to enable first iteration
		/// </summary>
		bool m_firstIterationHasOccured = false;

		/// <summary>
		/// Start
		/// </summary>
		protected virtual void Start()
		{
			firstIteration();
		}

		/// <summary>
		/// Called on first iteration
		/// </summary>
		protected abstract void firstIteration();

		/// <summary>
		/// Iteration
		/// </summary>
		protected abstract void iterate();

		/// <summary>
		/// Next iteration
		/// </summary>
		internal void nextIteration()
		{
			if (m_firstIterationHasOccured == true)
			{
				firstIteration();
			}
			else
			{
				iterate();
			}
		}

		/// <summary>
		/// Infects people
		/// </summary>
		protected abstract bool infect(PeopleBehavior p_target);

		/// <summary>
		/// Win / Lose conditions
		/// </summary>
		/// <returns></returns>
		internal abstract bool hasDeseaseWon();
		internal abstract bool hasDeseaseLost();

	}
}