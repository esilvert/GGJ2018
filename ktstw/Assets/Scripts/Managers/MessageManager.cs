/**
*	Created 01-27-2018
* 	Author : Vegevan
**/

using UnityEngine;
using UnityEngine.UI;

using Scripts.Gameplay;
using Scripts.Persistent;
using Scripts.UI;

using System;

namespace Scripts.GameManagers
{

    
	public class MessageManager : MonoBehaviour 
	{
        /// <summary>
        /// Messages container
        /// </summary>
        public Text _TextMessages = null; 

        /// <summary>
        /// One message height
        /// </summary>
        public int _MessageHeight = 16;

        [Header("New clue")]
        /// <summary>
        /// The text containing the new clue
        /// </summary>
        public GameObject _NewCluePanel = null;
        public Text _NewClueText = null;

        /// <summary>
        /// Next game panel
        /// </summary>
        public GameObject _NextGamePanel = null;
        public Text _SaveButtonErrorText = null;

        /// <summary>
        /// Next player description
        /// </summary>
        public Description _Description = null;


        /// <summary>
        /// Number of message
        /// </summary>
        int m_messageNumber = 0;
        internal int messageNumber {get {return m_messageNumber;}}

        /// <summary>
        /// Last action
        /// </summary>
        internal string lastAction;

        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            Debug.Assert(
                _TextMessages != null 
            &&  _NewCluePanel != null 
            &&  _NewClueText != null 
            &&  _NextGamePanel != null 
            &&  _SaveButtonErrorText != null 
            );

            hideNewcluePanel();
            _SaveButtonErrorText.gameObject.SetActive(false);
            
        }

        /// <summary>
        /// Adds a message
        /// </summary>
        internal void addMessage(string p_message)
        {
            RectTransform rtransform = _TextMessages.gameObject.GetComponent<RectTransform>();
            rtransform.SetSizeWithCurrentAnchors( RectTransform.Axis.Vertical, rtransform.rect.height + _MessageHeight) ;
            _TextMessages.text += (++m_messageNumber).ToString() + " : " + p_message + "\t(has " + lastAction + ")" +"\n";
        }

        /// <summary>
        /// Show the new clue panel
        /// </summary>
        internal void showNewCluePanel()
        {
            _NewCluePanel.SetActive(true);
            _Description.update();
        }

        /// <summary>
        /// Hide the new clue panel
        /// </summary>
        internal void hideNewcluePanel()
        {
            _NewCluePanel.SetActive(false);
        }

        /// <summary>
        /// Callback - Save pressed
        /// </summary>
        public void OnSavePressed()
        {
            // Reset
            _SaveButtonErrorText.text = "";

            
            // Sound
            SoundManager.instance.play(ESounds.Save);

            // Go
            bool hasError = false;

            if (_NewClueText.text.Length != 5)
            {
                hasError = true;
                _SaveButtonErrorText.text = "The message must be exactly FIVE characters long.\n";
            }

            foreach(char c in _NewClueText.text)
            {
                if (Char.IsDigit(c) == true)
                {
                    hasError = true;
                    _SaveButtonErrorText.text = "The clue can't have digit in it.";
                    break;
                }
            }

            if (hasError == false)
            {
                // Add message
                addMessage(_NewClueText.text);

                // Reset
                _NewClueText.text = "";
                hideNewcluePanel();

                // Show the next player panel
                _NextGamePanel.SetActive(true);
            }

            _SaveButtonErrorText.gameObject.SetActive(hasError);
            
        }

        /// <summary>
        /// Destroy => Save
        /// </summary>
        void OnDestroy()
        {
            PersistentDataManager.instance.messages = _TextMessages.text;
        }

        public void OnValueChanged()
        {
            // Sound
            SoundManager.instance.play(ESounds.Type);
        }
    
	}
}