/**
*	Created 01-26-2018
* 	Author : Vegevan
**/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using Scripts.Gameplay;

namespace Scripts.GameManagers
{

    
	public class DeseaseManager : MonoBehaviour 
	{
        /// <summary>
        /// Current desease (may change in future)
        /// </summary>
        public Desease _Desease = null;


        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            Debug.Assert(
                _Desease != null
            );
        }

        /// <summary>
        /// callback - Accept pressed
        /// </summary>
        public void OnAcceptPressed()
        {
            // Iterate
            _Desease.nextIteration();

            // Show spread
            _Desease._MapBehavior.reveal();
            
            if (_Desease.hasDeseaseWon() == true )
            {
                // GO TO LOSE SCREEN
                SceneManager.LoadScene("scn_lose", LoadSceneMode.Single);
            }
            else if (_Desease.hasDeseaseLost() == true )
            {
                // GO TO WIN SCREEN
                SceneManager.LoadScene("scn_win", LoadSceneMode.Single);
            }
        }
	}
}