﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.GameManagers
{
	internal enum ESounds
	{
		Click,
		Kill,
		Play,
		Save,
		Swipe,
		Type,
		Vaccin,
		Select
	}
	
	public class SoundManager : MonoBehaviour 
	{


			/// <summary>
			/// List of all sounds
			/// </summary>
			public List<AudioClip> _Sounds;

			/// <summary>
			/// Audio source
			/// </summary>
			AudioSource m_source;

			 static SoundManager m_instance;
			internal static SoundManager instance 
			{
				get 
				{
					if (m_instance == null)
					{
						GameObject obj = Object.Instantiate(Resources.Load("Level/sound_node")) as GameObject;
						// GameObject sound_node = Object.Instantiate(obj);
						m_instance = obj.GetComponent<SoundManager>();
						DontDestroyOnLoad(obj);
					}
					return m_instance;
				}
			}

			/// <summary>
			/// Start
			/// </summary>
			void Awake()
			{
				m_source = gameObject.GetComponent<AudioSource>();
			}

			/// <summary>
			/// Plays a sound
			/// </summary>
			internal void play(ESounds p_id)
			{
				m_source.clip = _Sounds[(int)p_id];
				m_source.Play();
			}

			internal void doNothing()
			{

			}
	}
}