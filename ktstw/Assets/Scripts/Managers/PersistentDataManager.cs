/**
*	Created 01-27-2018
* 	Author : Vegevan
**/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Scripts.Persistent
{
	public class PersistentDataManager
	{
        /// <summary>
        /// Log of all the messages / clues
        /// </summary>
        internal string messages;

        static PersistentDataManager m_instance;
        internal static PersistentDataManager instance {
                get 
                {
                    if (m_instance == null)
                        m_instance = new PersistentDataManager();
                    return m_instance;
                } 
            }

        /// <summary>
        /// Clear saved datas
        /// </summary>
        internal void clear()
        {
            messages = "";
        }
	}
}