/**
*	Created 01-26-2018
* 	Author : Vegevan
**/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.GameManagers
{

    
	public class Managers : MonoBehaviour 
	{
        /// <summary>
        /// Singleton
        /// </summary>
		static internal Managers instance {get; private set;}

        /// <summary>
        /// The input manager
        /// </summary>
        /// <returns></returns>
        internal InputManager inputManager {get; private set;}

        /// <summary>
        /// Desease manager
        /// </summary>
        /// <returns></returns>
        internal DeseaseManager deseaseManager {get; private set;}

        /// <summary>
        /// The message manager
        /// </summary>
        internal MessageManager messageManager {get; private set;}

        /// <summary>
        /// Sound manager
        /// </summary>
        /// <returns></returns>
        internal SoundManager soundManager {get; private set;}

        /// <summary>
        /// Awake
        /// </summary>
        void Awake()
        {
            // Singleton
            instance = this;

            // Input Manager
            inputManager = gameObject.GetComponent<InputManager>();

            // Desease Manager
            deseaseManager = GetComponent<DeseaseManager>();

            // Message Manager
            messageManager = GetComponent<MessageManager>();

            // Sound Manager
            soundManager = FindObjectOfType<SoundManager>();
        }
	}
}