/**
*	Created 01-27-2018
* 	Author : Vegevan
**/

using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using Scripts.GameManagers;
using Scripts.People;

namespace Scripts.GameManagers
{

    
	public class InputManager : MonoBehaviour 
	{
        ///
        enum EStates
        {
            Default,
            Killing,
            Blocked,
            Vaccin,

            Count
        }

        /// <summary>
        /// Action UI
        /// </summary>
        public Button _ActionButton = null;
        public Text _AcceptDisabledMessage = null;

        public Text _InformationMessage = null;

        /// <summary>
        /// Vaccin duration
        /// </summary>
        public int _VaccinDuration = 3;

        /// <summary>
        /// Selected color (feedback)
        /// </summary>
        public Color _KillingTargetColor = Color.yellow;
        public Color _VaccinTargetColor = Color.yellow;

        /// <summary>
        /// Input manager state
        /// </summary>
        EStates m_state;

        /// <summary>
        /// Target / selected people
        /// </summary>
        PeopleBehavior m_target = null;

        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            Debug.Assert(_ActionButton != null && _InformationMessage != null);

            // Init
            m_state = EStates.Default;
            setAcceptButtonAvailable(false, false);
        }

        /// <summary>
        /// Tells if bounds are selected
        /// </summary>
        internal bool isSelected(Bounds p_bounds)
        {
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z);

            bool touched = Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended && p_bounds.Contains(Input.GetTouch(0).position) == true ;
            bool clicked = Input.GetMouseButtonDown(0) && p_bounds.Contains(Camera.main.ScreenToWorldPoint(mousePosition));

            // Debug.LogFormat("Bounds {0}, Mouse {1}.", p_bounds, Camera.main.ScreenToWorldPoint(mousePosition));
            // Debug.LogFormat("Clicked {0}, Touched {1}.", clicked, touched);
            
            return touched || clicked;
        }

        internal void OnClicked(PeopleBehavior p_people)
        {
            SoundManager.instance.play(ESounds.Select);
            
            switch (m_state)
            {
                case EStates.Default:
                {
                    // Managers.
                }break;

                case EStates.Killing:
                {
                    if (m_target == null)
                    {
                        // New target
                        m_target = p_people;
                        p_people.renderer.color = _KillingTargetColor;
                    }
                    else
                    {
                        if (m_target == p_people)
                        {
                            // Un toggler
                            m_target = null;
                            p_people.renderer.color = p_people.baseColor;
                        }
                        else
                        {
                            // Switch target
                            m_target.renderer.color = p_people.baseColor;
                            m_target = p_people;
                            p_people.renderer.color = _KillingTargetColor;
                        }
                    }

                }break;

                case EStates.Vaccin:
                {
                    if (m_target == null)
                    {
                        // New target
                        m_target = p_people;
                        p_people.renderer.color = _VaccinTargetColor;
                    }
                    else
                    {
                        if (m_target == p_people)
                        {
                            // Un toggler
                            m_target = null;
                            p_people.renderer.color = p_people.baseColor;
                        }
                        else
                        {
                            // Switch target
                            m_target.renderer.color = p_people.baseColor;
                            m_target = p_people;
                            p_people.renderer.color = _VaccinTargetColor;
                        }
                    }

                }break;

                default: break;
            }

            setAcceptButtonAvailable(m_target != null, false);
        }

        /// <summary>
        /// Callback - Action pressed
        /// </summary>
        public void OnActionPressed()
        {
            switch (m_state)
            {
                case EStates.Default:
                {
                    // Managers.
                }break;

                case EStates.Killing:
                {
                    m_state = EStates.Blocked;

                    // Reset
                    m_target.gameObject.SetActive(false);
                    Managers.instance.messageManager.lastAction = "killed " + ((m_target.deseaseState > 0) ? "a sick person" : "an innocent");
                    m_target.GetComponent<PeopleBehavior>().setDeseaseState(-1);
                    showInformation("You have " + Managers.instance.messageManager.lastAction + " !");
                    m_target = null;
                    
                    // Sound
                    SoundManager.instance.play(ESounds.Kill);

                }break;

                case EStates.Vaccin:
                {
                    m_state = EStates.Blocked;

                    // Reset
                    PeopleBehavior pb = m_target.GetComponent<PeopleBehavior>();
                    
                    if (pb.deseaseState <= 0)
                    {
                        pb.turnsLeftVaccined = _VaccinDuration;
                        pb.OnVaccined();
                    }
                    
                    Managers.instance.messageManager.lastAction = "vaccined someone";

                    // De-select
                    pb.renderer.color = pb.baseColor;

                    m_target = null;
                    
                    // Sound
                    SoundManager.instance.play(ESounds.Vaccin);
                }break;

                default: break;
            }

            // Disable button
            setAcceptButtonAvailable(false, true);
            
            // Show new clue panel
            Managers.instance.messageManager.showNewCluePanel();

            // Turn has passed
            List<PeopleBehavior> allPeople = Managers.instance.deseaseManager._Desease._MapBehavior.getAllPeopleNotInState(-3);
            foreach(PeopleBehavior pb in allPeople)
            {
                pb.OnTurnOver();
            }
            
            // Spread the info
            Managers.instance.deseaseManager.OnAcceptPressed();
        }

        /// <summary>
        /// Enable or not the accept button
        /// </summary>
        internal void setAcceptButtonAvailable(bool p_value, bool p_goToMessage)
        {
            _ActionButton.interactable = p_value;
            _AcceptDisabledMessage.gameObject.SetActive(p_goToMessage);
        }

        /// <summary>
        /// Callback - Reset pressed
        /// </summary>
        public void OnResetPressed()
        {
            setAcceptButtonAvailable(true, false);
            _InformationMessage.gameObject.SetActive(false);
            m_state = EStates.Default;
        }

        /// <summary>
        /// Callback - kill pressed
        /// </summary>
        public void OnKillActionPressed()
        {
            if (m_state != EStates.Blocked)
            {
                if (m_target != null)
                    m_target.renderer.color = m_target.baseColor;
                m_target = null;
                m_state = EStates.Killing;
                
                // Sound
                SoundManager.instance.play(ESounds.Click);
            }
        }

        /// <summary>
        /// Callback - Vaccin pressed
        /// </summary>
        public void OnVaccineActionPressed()
        {
            if (m_state != EStates.Blocked)
            {
                if (m_target != null)
                    m_target.renderer.color = m_target.baseColor;
                m_target = null;
                m_state = EStates.Vaccin;
                
                // Sound
                SoundManager.instance.play(ESounds.Click);
            }
        }

        /// <summary>
        /// Show a message
        /// </summary>
        internal void showInformation(string p_message)
        {
            _InformationMessage.gameObject.SetActive(true);
            _InformationMessage.text = p_message;
        }
	}
}