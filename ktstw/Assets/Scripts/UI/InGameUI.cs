/**
*	Created 01-26-2018
* 	Author : Vegevan
**/

using UnityEngine;
using UnityEngine.UI;
using Scripts.GameManagers;

namespace Scripts.UI
{
	public class InGameUI : MonoBehaviour 
	{   
        /// <summary>
        /// UI animator
        /// </summary>
        public Animator _UIAnimator = null;

        /// <summary>
        /// Next game panel
        /// </summary>
        public GameObject _NextGamePanel = null;

        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            Debug.Assert(_UIAnimator != null && _NextGamePanel != null);
        
            _NextGamePanel.SetActive(false);

            SoundManager.instance.doNothing();
        }

        /// <summary>
        /// Callback - On messages clicked
        /// </summary>
        public void OnMessagesClicked()
        {
            _UIAnimator.Play("InGameToMessages");
        }

        /// <summary>
        /// Callback - On actions clicked
        /// </summary>
        public void OnActionsClicked()
        {
            _UIAnimator.Play("MessagesToInGame");
        }

        /// <summary>
        /// Callback - On tutorial clicked
        /// </summary>
        public void OnTutorialClicked()
        {
            _UIAnimator.Play("MessagesToTutorial");
        }

        /// <summary>
        /// Callback - On tutorial clicked
        /// </summary>
        public void OnMessagesFromTutorialClicked()
        {
            _UIAnimator.Play("TutorialToMessages");
        }

        /// <summary>
        /// Callback - Go pressed
        /// </summary>
        public void OnGoPressed()
        {
            _NextGamePanel.SetActive(false);

            // Enable action button
            Managers.instance.inputManager.setAcceptButtonAvailable(true, false);

            // Hide spread
            Managers.instance.deseaseManager._Desease._MapBehavior.hide();

            // Sound
            SoundManager.instance.play(ESounds.Play);

            // Animator
            // _UIAnimator.Play("MessagesToInGame"); // Quite confusing
        }


	}
}