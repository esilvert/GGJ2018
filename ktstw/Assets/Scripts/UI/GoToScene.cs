/**
*	Created 01-27-2018
* 	Author : Vegevan
**/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using Scripts.GameManagers;

namespace Scripts.UI
{
	public class GoToScene : MonoBehaviour 
	{
        public string _Scene = "scn_";
        internal void goToScene()
        {
            SceneManager.LoadScene(_Scene);
        }
	}
}