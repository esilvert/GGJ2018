/**
*	Created 01-27-2018
* 	Author : Vegevan
**/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Scripts.GameManagers;

namespace Scripts.UI
{
	public class SplashscreenUI : MonoBehaviour 
	{   
        /// <summary>
        /// UI animator
        /// </summary>
        public Animator _UIAnimator = null;

        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            Debug.Assert(_UIAnimator != null);

            SoundManager.instance.doNothing();
        }

        /// <summary>
        /// Callback - On messages clicked
        /// </summary>
        public void OnCreditsClicked()
        {
            _UIAnimator.Play("StartToCredits");
        }

        /// <summary>
        /// Callback - On tutorial clicked
        /// </summary>
        public void OnTutorialClicked()
        {
            _UIAnimator.Play("StartToTutorial");
        }

        /// <summary>
        /// Callback - On tutorial clicked
        /// </summary>
        public void OnStartFromTutorialClicked()
        {
            _UIAnimator.Play("StartFromTutorial");
        }

        /// <summary>
        /// Callback - On tutorial clicked
        /// </summary>
        public void OnStartFromCreditsClicked()
        {
            _UIAnimator.Play("StartFromCredits");
        }

        /// <summary>
        /// Callback - On Play clicked
        /// </summary>
        public void OnPlayClicked()
        {
            SoundManager.instance.play(ESounds.Click);
            SceneManager.LoadScene("scn_game", LoadSceneMode.Single);
        }

	}
}