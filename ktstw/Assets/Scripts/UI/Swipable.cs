﻿/**
*	Created 01-27-2018
* 	Author : Vegevan
**/


using UnityEngine;

using Scripts.GameManagers;

namespace Scripts.UI
{

	public class Swipable : MonoBehaviour 
	{
			/// <summary>
			/// The swipable object
			/// </summary>
			public GameObject _Swipable = null;

			/// <summary>
			/// Sensibility
			/// </summary>
			public Vector2 _Sensitivity = Vector2.one;

			/// <summary>
			/// Steps
			/// </summary>
			public Vector2 _Step = Vector2.one * 32;

			/// <summary>
			/// Max step number
			/// </summary>
			public Vector2 _MaxStepX = Vector2.zero;
			public Vector2 _MaxStepY = Vector2.zero;

			/// <summary>
			/// Time to reposition
			/// </summary>
			public float _RepositioningDelay = 0.3f;

			public float _TriggerRatio = 0.5f;

			/// <summary>
			/// Use to move smoothly
			/// </summary>
			Vector2 m_remaining;
			float m_releaseTimestamp;
			Vector3 m_startPosition;
			Vector3 m_originPosition;
			Vector3 m_releasePosition;

			bool m_repositionning;

			/// <summary>
			/// Start
			/// </summary>
			void Start()
			{
				Debug.Assert(_Swipable != null);

				// Init
				m_releaseTimestamp = float.NegativeInfinity;
				m_releasePosition = _Swipable.transform.position;
				m_remaining = Vector2.zero;
				m_repositionning = false;

				// 
				m_originPosition = transform.position;
			}

			/// <summary>
			/// Update
			/// </summary>
			void Update()
			{
				if (  m_repositionning == true)
				{
					_Swipable.transform.position = Vector3.Lerp(
							m_releasePosition,
							m_releasePosition + (Vector3)m_remaining,
							Mathf.Clamp01( (Time.unscaledTime - m_releaseTimestamp) / _RepositioningDelay));

					m_repositionning = Mathf.Clamp01( (Time.unscaledTime - m_releaseTimestamp) / _RepositioningDelay) != 1;
				}
				else if (Input.touchCount > 0 )
				{
					if (Input.GetTouch(0).phase == TouchPhase.Began) 
					{
						m_startPosition = _Swipable.transform.position;

					}
					else if (Input.GetTouch(0).phase == TouchPhase.Moved) 
					{
						Vector2 delta = Input.GetTouch(0).deltaPosition;
						_Swipable.transform.Translate ( new Vector3(_Sensitivity.x * delta.x, _Sensitivity.y * delta.y) );
						// Debug.LogFormat("Delta = {0}", delta);
					}
				
					else if (Input.GetTouch(0).phase == TouchPhase.Ended) 
					{
						SoundManager.instance.play(ESounds.Swipe);
						m_remaining.x = - (_Swipable.transform.position.x - m_startPosition.x);
						if (Mathf.Abs(m_remaining.x) > _Step.x * _TriggerRatio) m_remaining.x -= Mathf.Sign(m_remaining.x) * _Step.x;

						int totalShiftX = (int)((m_originPosition.x - _Swipable.transform.position.x - m_remaining.x) / _Step.x);
						if (totalShiftX > (int)_MaxStepX.y || totalShiftX < (int)_MaxStepX.x) 	 m_remaining.x -= Mathf.Sign(m_remaining.x) * _Step.x;

						m_remaining.y = - ( _Swipable.transform.position.y - m_startPosition.y);
						if (Mathf.Abs(m_remaining.y) > _Step.y * _TriggerRatio) m_remaining.y -= Mathf.Sign(m_remaining.y) * _Step.y;

						int totalShiftY = (int)((m_originPosition.y - _Swipable.transform.position.y - m_remaining.y) / _Step.y);
						if (totalShiftY > (int)_MaxStepY.y || totalShiftY < (int)_MaxStepY.x) 	 m_remaining.y -= Mathf.Sign(m_remaining.y) * _Step.y;
						


						m_releaseTimestamp = Time.unscaledTime;
						m_releasePosition = _Swipable.transform.position;
						m_repositionning = true;
					}
				}
			}
	}
}