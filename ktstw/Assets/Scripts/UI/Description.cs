/**
*	Created 01-26-2018
* 	Author : Vegevan
**/

using UnityEngine;
using UnityEngine.UI;

using Scripts.Level;
using Scripts.People;

using System.Collections;
using System.Collections.Generic;

namespace Scripts.UI
{
	public class Description : MonoBehaviour 
	{
		public List<string> _Descriptions = null;
        public Text _DescriptionText = null;

        internal void update()
        {
            string rdm = _Descriptions[Random.Range(0, _Descriptions.Count)]; 
            _DescriptionText.text = rdm;
        }
	}
}