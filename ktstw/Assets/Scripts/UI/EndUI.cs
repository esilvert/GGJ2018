/**
*	Created 01-27-2018
* 	Author : Vegevan
**/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Scripts.GameManagers;
using Scripts.Persistent;

namespace Scripts.UI
{
	public class EndUI : MonoBehaviour 
	{   
        public Text _Text;

        void Start()
        {
            _Text.text = PersistentDataManager.instance.messages;
        }

        void Destroy()
        {
           PersistentDataManager.instance.clear();
        }

	}
}