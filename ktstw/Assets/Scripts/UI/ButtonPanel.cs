/**
*	Created 01-27-2018
* 	Author : Vegevan
**/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace Scripts.UI
{
	public class ButtonPanel : MonoBehaviour 
	{
        public ColorBlock _Selected, _NonSelected;

        Button[] m_buttons;
       public void OnPressed()
       {
           foreach(Button button in m_buttons)
           {
               if (button.gameObject == EventSystem.current.currentSelectedGameObject)
               {
                   button.colors = _Selected;
               }
               else
               {
                   button.colors = _NonSelected;
               }
           }
       }

       void Start()
       {
           m_buttons = GetComponentsInChildren<Button>();
           foreach(Button button in m_buttons)
           {
               button.onClick.AddListener(OnPressed);
               button.colors = _NonSelected;
           }
       }

       public void disable()
       {
           foreach(Button button in m_buttons)
           {
               button.colors = _NonSelected;
               button.interactable = false;
           }
       }

        public void reset()
       {
           foreach(Button button in m_buttons)
           {
               button.interactable = true;
           }
       }
	}
}