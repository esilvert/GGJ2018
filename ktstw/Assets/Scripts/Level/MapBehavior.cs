﻿/**
*	Created 01-26-2018
* 	Author : Vegevan
**/

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using UnityEngine.Tilemaps;

using Scripts.People;

namespace Scripts.Level
{

	public class MapBehavior : MonoBehaviour 
	{
		/// <summary>
		/// Where people are gathered
		/// </summary>
		public GameObject _PeopleContainer = null;


		/// <summary>
		/// People Prefab
		/// </summary>
		public GameObject _PeoplePrefab = null;

		/// <summary>
		/// Dimensions
		/// </summary>
		public Vector2 _Origin = new Vector2(0, 0);
		public Vector2 _Dimensions = new Vector2(10, 10);

		/// <summary>
		/// Per cell probability to spawn a people
		/// </summary>
		[Range(0f,1f)]
		public float _SpawnProbabilityPerCell = 0.3f;

		/// <summary>
		/// The used tiles
		/// </summary>
		TileBase[] m_usedTiles;

		/// <summary>
		/// The tilemap
		/// </summary>
		Tilemap m_map;

		/// <summary>
		/// People Grid
		/// </summary>
		Dictionary<Vector2, PeopleBehavior> m_peopleGrid;
		List<PeopleBehavior> m_peopleList;

		/// <summary>
		/// Start
		/// </summary>
		void Awake()
		{
			// Safety
			Debug.Assert(_PeopleContainer);

			// Init
			m_peopleGrid = new Dictionary<Vector2, PeopleBehavior>((int)(_Dimensions.x * _Dimensions.y));
			m_peopleList = new List<PeopleBehavior>((int)(_Dimensions.x * _Dimensions.y));

			// Get Tilemap
			m_map = GetComponent<Tilemap>();
			
			// Init array
			m_usedTiles = new TileBase[m_map.GetUsedTilesCount()];
			m_map.GetUsedTilesNonAlloc(m_usedTiles);

			generatePeople();
			
		}

		/// <summary>
		/// Generates the peoples
		/// </summary>
		void generatePeople()
		{
			for (int y = (int)-_Origin.y; y > -_Origin.y -_Dimensions.y; --y)
			{
				for (int x =  (int)_Origin.x ; x < _Origin.x + _Dimensions.x ; ++x)
				{
					// People ?
					if (Random.Range(0f,1f) < _SpawnProbabilityPerCell)
					{
						GameObject people = GameObject.Instantiate(_PeoplePrefab );
						setPeopleAtPosition(people, new Vector3Int(x,-y,0));
						people.transform.SetParent(_PeopleContainer.transform);

						PeopleBehavior behavior = people.GetComponent<PeopleBehavior>();
						behavior.mapPosition = new Vector2(x,-y);
						// Debug.LogFormat("Added someone at {0}", new Vector2(x,y));
						m_peopleGrid.Add( new Vector2(x, -y), behavior );
						m_peopleList.Add(behavior);
					}
				}
			}
		}

		/// <summary>
		/// Set to position
		/// </summary>
		void setPeopleAtPosition(GameObject p_obj, Vector3Int p_pos)
		{
			p_obj.transform.position = m_map.CellToWorld( new Vector3Int(p_pos.x, -p_pos.y, p_pos.z)) + new Vector3(0.16f * transform.localScale.x, -0.16f * transform.localScale.y,0); // WARNING 32px tile
		}

		
		/// <summary>
		/// Returns a random people
		/// </summary>
		/// <returns></returns>
		internal PeopleBehavior getRandomPeople(int p_state)
		{
			List<PeopleBehavior> candidates = getAllPeopleWithState(p_state);

			return candidates.Count == 0 ? null : candidates[Random.Range(0, candidates.Count)];
		}

		/// <summary>
		/// Returns a list of people in this state
		/// </summary>
		internal List<PeopleBehavior> getAllPeopleWithState(int p_state)
		{
			List<PeopleBehavior> result = new List<PeopleBehavior>(m_peopleList.Count);

			foreach(PeopleBehavior people in m_peopleList)
			{
				if (people.deseaseState == p_state && people.gameObject.activeSelf == true)
					result.Add(people);
			}
			
			return result;
		}

		/// <summary>
		/// Returns a list of people in this state
		/// </summary>
		internal List<PeopleBehavior> getAllPeopleNotInState(int p_state)
		{
			List<PeopleBehavior> result = new List<PeopleBehavior>(m_peopleList.Count);

			foreach(PeopleBehavior people in m_peopleList)
			{
				if (people.deseaseState != p_state && people.gameObject.activeSelf == true)
					result.Add(people);
			}
			
			return result;
		}

		/// <summary>
		/// Returns all the neighbors of a people
		/// </summary>
		internal List<PeopleBehavior> getNeighbors(PeopleBehavior p_origin)
		{
			List<PeopleBehavior> result = new List<PeopleBehavior>(4);

			// Neighbor list
			Vector2[] neighborsPosition = new Vector2[4]{
					 	p_origin.mapPosition + Vector2.left,
						p_origin.mapPosition + Vector2.right,
						p_origin.mapPosition + Vector2.down,
					 	p_origin.mapPosition + Vector2.up,
			}; 
			
			// Fill
			foreach(Vector2 neighbor in neighborsPosition)
			{
				if (m_peopleGrid.ContainsKey(neighbor) == true)
				{
					result.Add(m_peopleGrid[neighbor]);
				}
				else
					result.Add(null);
			}
			
			// return
			return result; // has 4 elements
		}

		/// <summary>
		/// Return people count
		/// </summary>
		internal int getPeopleCount()
		{
			int res = 0;

			foreach(PeopleBehavior pb in m_peopleList)
			{
				if (pb.gameObject.activeSelf == true)
					++res;
			}
			
			return res;
		}

		/// <summary>
		/// Hide
		/// </summary>
		internal void hide()
		{
			foreach(PeopleBehavior pb in m_peopleList)
			{
				if (pb.gameObject.activeSelf == true)
					pb.hide();
			}
		}

		/// <summary>
		/// Reveal
		/// </summary>
		internal void reveal()
		{
			foreach(PeopleBehavior pb in m_peopleList)
			{
				if (pb.gameObject.activeSelf == true)
					pb.reveal();
			}
		}

		/// <summary>
		/// Moves a people from to
		/// </summary>
		internal void movePeople(Vector2 p_start, Vector2 p_end)
		{
			if (m_peopleGrid.ContainsKey(p_start) == false)
			{
				// WTF ?!
				return;
			}
			PeopleBehavior origin = m_peopleGrid[p_start];

			if (m_peopleGrid.ContainsKey(p_end))
			{
				// We fucked up...
				origin.gameObject.SetActive(false);
				return;
			}
			
			origin.mapPosition = p_end;
			setPeopleAtPosition(origin.gameObject, new Vector3Int((int)p_end.x, (int)p_end.y, 0));
			m_peopleGrid.Add(p_end, origin);
			m_peopleGrid.Remove(p_start);
		}
	}
}